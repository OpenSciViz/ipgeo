#!/usr/bin/python3

import urllib.request
import json

geo_url = "https://geolocation-db.com/json/"
google_ip = ['8.8.8.8', '8.8.4.4']

with urllib.request.urlopen(geo_url) as url:
  data = json.loads(url.read().decode())
  print(data)

for idx in range(2):
  with urllib.request.urlopen(geo_url+google_ip[idx]) as url:
    data = json.loads(url.read().decode())
    print(data)

