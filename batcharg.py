#!/usr/bin/env python3 
#!/usr/bin/env python  # cygwin11
# Battery Notifier
# pip install ...
from plyer import notification as notice
import psutil, signal, sys, time

def sig_handler(signum, stack_frame):
  msg = f"sig_handler> {signum}"
  print(msg)
  sys.exit(1)

signal.signal(signal.SIGINT, sig_handler)

pause = 3.0
while True:
  battery = psutil.sensors_battery()
  try:
    life = battery.percent
    notice.notify(title='Battery', message=f"% battery charge {life}",timeout=10)
    msg = f"battery charge {life}" ; print(msg)
    msg = f"% battery notification via plyer: {life} ... sleep a bit before next notice..."
    print(msg) ; time.sleep(pause)
  except:
    msg = f"battery charge percent feature not supported ... not a laptop?"
    print(msg) ; time.sleep(pause)

