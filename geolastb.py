#!/usr/bin/python3

import copy, os, platform, psutil, sys
import pexpect, subprocess
from datetime import datetime as dtm
#from cliar import Cliar

def blogons():
  """ 
  blocked logon attempts are available via lastb CLI to root ONLY.
  """
  import re
  print("="*40, "Boot Time", "="*40)
  print("blocked logon attempts are available via lastb CLI only to root.")

# pexpect does not support spawned sub-shell pipes simply?
# child = pexpect.spawn('lastb|sort -u') 
# child = pexpect.spawn('lastbi;) # than perform sort -u 
# child.expect(pexpect.EOF)
# blocked_logons = child.before
# child.close()

# run lastb as root via subshell that pipes to awk and sort -u
  blocked_IPs = subprocess.check_output("lastb|awk '{print $3}'|sort -u", shell=True) # just unique IPs
  iptxt = f'{blocked_IPs.decode("utf-8")}' ; print(iptxt)
  iplist = iptxt.splitlines()
  return iplist # list of blocked IPs

# blocked_logons = subprocess.check_output("lastb|awk '{print $1, $3}'|sort -u", shell=True) # account names and IPs
# ac_iptxt = f'{blocked_logons.decode("utf-8")}' ; print(ac_iptxt) # ensure this is a list
# words = re.split(r'\s{2,}', ac_iptxt) ; print(words[0], words[1])
# ac_iptxt = words

# return list of strings with each string containing two words separated by some kinda whitespace(s) "account_name IP"
# ac_iptxt[:0] = 'google_88dns 8.8.8.8'; ac_iptxt[:0] ='google_84dns  8.8.4.4' # prepend sanity checks 
# return ac_iptxt # list of blocked IP strings should be 2 cols == ["account IP","another_account another_IP",...] 

#end blogins()

def isIP4(item='012.345.678.987'):
  import re
  if not(item): return False
  match = re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", item)
  return bool(match)

def ticks(cnt=6):
  import time
  print("..."*10," sleep betwixt ipapi.co requests to avoid exceeding daily 30k limit. cnt ==", cnt, "..."*10)
  while(cnt):
    time.sleep(1.0) ; print(" ... "*cnt) ; sys.stdout.flush() ; cnt -= 1
  return cnt

#def geo_ip(iplist=["root IPv4", "user 000.000.000.000", "guest 255.255.255.255"]):
def geo_ip(iplist=['8.8.8.8', '8.8.4.4']):
  """
  given array of str, each string an IP, use ipapi or open-street-maps nominatum
  to find geolocation of each IP and append to str
  ipai.co allows 30k / month free requests -- 1k / day ... once every 90 sec == 960 should be free
  sanity test/check via google dns IPs
  """
  import json, re, urllib.request as urlreq

  google_dns = ['8.8.8.8', '8.8.4.4'] ; print("sanity check/test: always include google DNS IPs:", google_dns)
  if(iplist): google_dns.extend(iplist)
  iplist = google_dns

  ipjson_list = [] # copy.deepcopy(iplist)
  cnt = 0
  for item in iplist:
    if not(isIP4(item)):
      print("..."*10," not an IP? item ==", item, " ... skipping ...", "..."*10) ; continue # skip bogus IPs
    ip_url = 'https://ipapi.co/' + item + '/json/'
    json_txt = urlreq.urlopen(ip_url).read()
    ipjson_list.append(json_txt) ; print(json_txt)
    cnt += 1
    if not(cnt % 10): ticks()
# endfor
  return ipjson_list

def main(args, json_content=[]):
  import sysinfo
  # test spawn
  #child = pexpect.spawn('date')
  #child.expect(pexpect.EOF)
  #dt = child.before
  #child.close()
  dt = sysinfo.dtm_now()
  print(dt)
  txt = f'Today is : {dt}' ; print(txt)
  # compare today date with boot datetime
  bt = sysinfo.boot() ; print(bt)

  # unique blocked logons
  blockedIPs = blogons()
  json_content = geo_ip(blockedIPs)
  conlen = len(json_content)
  if not conlen:
    printf("sorry sumthin failed; goodbye...") ; sys.exit(-1)

  txt = f'total count of blocked logons: {conlen}' ; print(txt)
  return txt 

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

