#!/usr/bin/env python3
import boto3

def aws_prices(client, srvcode='AmazonEC2', maxresults=13):
  """
  return current prices of aws services ... TBD verify params
  from https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/pricing.html
  """
  res_srvs = client.describe_services(FormatVersion='aws_v1',ServiceCode=srvcode,MaxResults=maxresults)
  print(res_srvs)
  return(res_srvs)


def aws_attribs(client, isrvcode='AmazonEC2', maxreults=13):
  """
  return current prices of aws services ... TBD verify attributes params
  from https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/pricing.html
  """
  res_vals = client.get_attribute_values(AttributeName='volumeType',ServiceCode=srvcode,MaxResults=maxresults)
  print(res_vals)
  return(res_vals)

def aws_products(client, maxresults=13):
  """
  return current prices of aws services ... TBD verify productss params
  from https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/pricing.html
  """
  res_prods = client.get_products(Filters=[{'Field': 'ServiceCode','Type': 'TERM_MATCH','Value': 'AmazonEC2'},
                                           {'Field': 'volumeType','Type': 'TERM_MATCH','Value': 'Provisioned IOPS'}],
                                           FormatVersion='aws_v1',MaxResults=maxresults)
  print(res_prods)
  return(res_prods)

def main():
  """
  from https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/pricing.html

  use boto3 pricing module and/or fetch json content and parse.
  offer_idx = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.json'
  ec2_prices = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/us-east-1/index.json'
  
  aws price list service api provides the following two endpoints:
  https://api.pricing.us-east-1.amazonaws.com
  https://api.pricing.ap-south-1.amazonaws.com

  sudo apt install python-dev-is-python3 -- provides pdb
  TBD -- find complete list of service codes and client object features
  """

  client = boto3.client('pricing')
  attribs = aws_attribs(client)
  prices = aws_prices(client)
  prods = aws_products(client)

  return {'attribs':attribs, 'prices':prices, 'prods':prods}

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  aws_info = main(args) ; print(aws_info)
  sys.exit(0)

