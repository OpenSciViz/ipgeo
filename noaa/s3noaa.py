#!/usr/bon/env python

def himawari9(hurl='https://noaa-himawari9.s3.amazonaws.com/index.html#AHI-L1b-Target/2023/01/01/'):
  #url_himawari9 = 'https://noaa-himawari9.s3.amazonaws.com/index.html#AHI-L1b-Target/2023/01/01/{0000,0010,0020,...0810}'
  base_url = []
  for item in range(10,820,10):
    sitem = str(item)
    url = hurl + '00' + sitem
    if(item > 100):
      url = hurl + '0' + sitem
    if(item > 1000):
      url = hurl + sitem

    url += '/' ; # print(url)
    base_url.append(url)
    ucnt = len(base_url) ; print("url cnt:", ucnt)
  return base_url

def main(args):
  hurls = himawari9()
  print(hurls)

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

