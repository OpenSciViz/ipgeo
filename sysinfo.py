#!/usr/bin/python3

import copy, os, platform, psutil, sys, pexpect
from datetime import datetime as dtm
#from cliar import Cliar

def hello(name='world', shout=False):
  """hello world or arg-name"""
  greeting = f"Hello {name}!"
  print(greeting.upper() if shout else greeting)
#end hello()

def dtm_now():
  now = dtm.now()
  return now.strftime("%Y/%m/%d/%H:%M:%S")

def uname():
  print("="*40, "System Information", "="*40)
  unam = platform.uname()
  print(f"System: {unam.system}")
  print(f"Node Name: {unam.node}")
  print(f"Release: {unam.release}")
  print(f"Version: {unam.version}")
  print(f"Machine: {unam.machine}")
  print(f"Processor: {unam.processor}")
  return unam
#end uname()

def boot():
  """Boot Time"""
  print("="*40, "Boot Time", "="*40)
  boot_time_timestamp = psutil.boot_time()
  bt = dtm.fromtimestamp(boot_time_timestamp)
  boot_datetime = f"Boot Time: {bt.year}/{bt.month}/{bt.day} {bt.hour}:{bt.minute}:{bt.second}"
  return boot_datetime
#end boot()

def cpu():
  """
  number of CPU cores CPU frequencies CPU usage
  """
  import psutil
  totcpu = psutil.cpu_count(logical=True)
  cpucnt = psutil.cpu_count(logical=False)
  print("="*40, "CPU Info", "="*40)
  print("Physical cores:", cpucnt)
  print("Total (logical) cores:", totcpu)
  cpufreq = psutil.cpu_freq()
  print(f"Max Frequency: {cpufreq.max:.2f}Mhz")
  print(f"Min Frequency: {cpufreq.min:.2f}Mhz")
  print(f"Current Frequency: {cpufreq.current:.2f}Mhz")
  print("CPU Usage Per Core:")
  for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
    print(f"Core {i}: {percentage}%")
  print(f"Total CPU Usage: {psutil.cpu_percent()}%") 
  return totcpu
#end cpu() 

def isIP4(item='012.345.678.987'):
  import re
  if not(item): return False
  match = re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", item)
  return bool(match)

def ticks(cnt=6):
  import time
  while(cnt):
    time.sleep(1.0) ; print(" ... "*cnt) ; sys.stdout.flush() ; cnt -= 1
  return cnt

def main(args, json_content=[]):
  # test spawn
  child = pexpect.spawn('date')
  child.expect(pexpect.EOF)
  dt = child.before
  child.close()
  txt = f'Today is : {dt.decode("utf-8")}' ; print(txt)
  bt = boot() ; print(bt)
  u = uname()
  c = cpu()

if __name__ == "__main__":
  args = []
  if len(sys.argv) > 1: args = copy.deepcopy(sys.argv)
  res = main(args)
  sys.exit(0)

